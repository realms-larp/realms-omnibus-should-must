::::: {.suggestion .block .part-2}
### [2.1: Participant Rights](./Omnibus.html#participant-rights) ###

If an EH subjects a player to a consequence at their event they [should]{.highlight} notify
all other EHs of this via the Event Holders' List. The player involved must
receive a copy of this notification and may send a formal response. People who
want to bring formal charges should report such charges to the Arbitration
Committee (<arbitration@lists.realmsnet.net>). In addition, any participant that
feels wrongful action was taken against them can bring the issue before the
[Arbitration Committee](./Omnibus.html#arbitration-committee).

<details class="edit" open><summary></summary>
If an EH subjects a player to a consequence at their event they ~~should~~
[are encouraged to]{.new} notify all other EHs of this via the Event Holders'
List.
</details>
:::::

::::: {.suggestion .block .part-2}
### [2.1: Participant Rights](./Omnibus.html#participant-rights) ###

If an EH subjects a player to a consequence at their event they should notify
all other EHs of this via the Event Holders' List. The player involved must
receive a copy of this notification and may send a formal response. People who
want to bring formal charges [should]{.highlight} report such charges to the Arbitration
Committee (<arbitration@lists.realmsnet.net>). In addition, any participant that
feels wrongful action was taken against them can bring the issue before the
[Arbitration Committee](./Omnibus.html#arbitration-committee).

<details class="edit" open><summary></summary>
People who want to bring formal charges ~~should report~~ [can do so by
reporting]{.new} such charges to the Arbitration Committee
(<arbitration@lists.realmsnet.net>).
</details>
:::::

::::: {.suggestion .block .part-2}
### [2.1: Participant Rights](./Omnibus.html#participant-rights) ###

Reports of rules violations to be submitted after the conclusion of an event
[should]{.highlight} be submitted to the Arbitration Committee.

<details class="edit" open><summary></summary>
Reports of rules violations to be submitted after the conclusion of an event
~~should~~ [can]{.new} be submitted to the Arbitration Committee.
</details>
:::::

::::: {.rule .block .part-2}
### [2.2: Code of Conduct](./Omnibus.html#code-of-conduct) ###

If you have reason to believe rules regarding the Code of Conduct may be
violated at an event, you [should]{.highlight} notify the Event Holder prior to the event
occurring.

<details class="edit" open><summary></summary>
If you have reason to believe rules regarding the Code of Conduct may be
violated at an event, ~~you should~~ notify the Event Holder prior to the event
occurring.
</details>
:::::

::::: {.rule .block .part-2}
### [2.2: Code of Conduct](./Omnibus.html#code-of-conduct) ###

The following list is not meant to be exhaustive, and any non-consensual
violations of a participant's rights [should]{.highlight} be considered a violation of the
rules.

<details class="edit" open><summary></summary>
and any non-consensual violations of a participant's rights ~~should be~~
[are]{.new} considered ~~a violation~~ [violations]{.new} of the rules.
</details>
:::::

::::: {.rule .block .part-2}
### [2.2: Code of Conduct](./Omnibus.html#code-of-conduct) ###

Role-play can never be used as an excuse for any of these behaviors. If you are
informed that your role-playing is OOC unsafe, threatening, or is not consented
to by other participants you [should]{.highlight} stop immediately and find another way to
play out the scene.

<details class="edit" open><summary></summary>
If you are informed that your role-playing is OOC unsafe, threatening, or is not
consented to by other participants[,]{.new} you ~~should~~ [must]{.new} stop
immediately and find another way to play out the scene.
</details>
:::::

::::: {.rule .block .part-2}
### [2.3: The Safety Rules](./Omnibus.html#the-safety-rules) ###

The safety rules are out-of-character (hereafter referred to as OOC). They are
for our safety, and provide the guidelines that we [should]{.highlight} all be playing by.

<details class="edit" open><summary></summary>
They are for our safety, and [they]{.new} provide the guidelines that we
~~should~~ all ~~be playing~~ [must play]{.new} by.
</details>
:::::

::::: {.ideal .block .part-2}
### 2.3 &sect; [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

1. We [should]{.highlight} all be doing this to have fun. If you get mad or uncontrolled, it
   is up to you to remove yourself from the game until you have regained your
   composure.
:::::

::::: {.suggestion .block .part-2}
### 2.3 &sect; [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

5. HOLD: If you see a harmful or unsafe situation (e.g., someone is about to run
   into a tree, gets their glasses knocked off, has had their weapon really
   broken in combat, is about to fall off a cliff, etc.) yell the word "Hold."
   If someone is injured, it is the primary responsibility of the person who is
   hurt to call a Hold. Before calling a Hold for someone else in an otherwise
   safe situation, you must first ask if they are all right. Holds [should]{.highlight} only
   be called in the event of a dangerous situation, and should never be used to
   discuss the rules. If you hear the word "Hold," stop immediately, then say
   "Hold" until everyone else has stopped moving. Once the emergency has been
   dealt with, a "Lay-On" (continue play) will be called either by a qualified
   marshal, or the person who originally called the Hold. Do not resume play
   until a Lay-On has been called.

<details class="edit" open><summary></summary>
Holds ~~should~~ [are]{.new} only [to]{.new} be called in the event of a
dangerous situation,
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

5. HOLD: If you see a harmful or unsafe situation (e.g., someone is about to run
   into a tree, gets their glasses knocked off, has had their weapon really
   broken in combat, is about to fall off a cliff, etc.) yell the word "Hold."
   If someone is injured, it is the primary responsibility of the person who is
   hurt to call a Hold. Before calling a Hold for someone else in an otherwise
   safe situation, you must first ask if they are all right. Holds should only
   be called in the event of a dangerous situation, and [should]{.highlight} never be used to
   discuss the rules. If you hear the word "Hold," stop immediately, then say
   "Hold" until everyone else has stopped moving. Once the emergency has been
   dealt with, a "Lay-On" (continue play) will be called either by a qualified
   marshal, or the person who originally called the Hold. Do not resume play
   until a Lay-On has been called.

<details class="edit" open><summary></summary>
and ~~should~~ [may]{.new} never be used to discuss the rules.
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

6. Only weapons and shields that have been made following the [construction
   guidelines](./Omnibus.html#weapon-construction) are
   to be used. Weapons [should]{.highlight} be checked between combat situations to ensure
   continued safety. A qualified marshal may be requested to check the safety of
   any weapons or shields at any time. Any new designs or materials must be
   inspected and approved by the EH or a designated marshal before use.

<details class="edit" open><summary></summary>
Weapons ~~should~~ [need to]{.new} be checked between combat situations to
ensure continued safety.
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Play By](./Omnibus.html#the-rules-we-play-by) ###

10. While the wording of the game rules may occasionally be less than clear,
    players [should]{.highlight} not use any such confusion to their own advantage. A simple
    guideline is to not assume any benefits unless they are specifically granted
    by a rule. If you feel that the way a rule is written grants you an advantage
    by omission of a statement to the contrary, you must review that rule with a
    marshal or EH prior to utilizing that advantage. While this document has a
    lot of general rules (such as [Basic Magic Effects Everyone
    Should Know](./Omnibus.html#basic-magic-effects-everyone-should-know)), any specific rule
    will always override these general rules.

<details class="edit" open><summary></summary>
players ~~should~~ [may]{.new} not use any such confusion to their own
advantage.
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

11. This is a lightest touch sport. ANY contact with a weapon to a body is to be
    taken as a hit. Ignoring a "light" blow is cheating and a marshal may remove
	you from the fight. There are to be NO full-strength swings. A marshal may
	remove you for excessive blow strength. Weapons, melee and missile, [should]{.highlight}
	be used with the minimum force necessary to score a successful hit.

<details class="edit" open><summary></summary>
Weapons, melee and missile, ~~should~~ [must]{.new} be used with the minimum
force necessary to score a successful hit.
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

15. Shields are for blocking ONLY. Your shield [should]{.highlight} never be used as a weapon.
    Shield-bashing or other shield contact with another person is unsafe.

<details class="edit" open><summary></summary>
Your shield ~~should~~ [must]{.new} never be used as a weapon.
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

17. Do NOT ever throw a weapon at a participant, unless that weapon is of a type
    (magic missile, javelin, or lightning bolt) sanctioned by the rules for
    throwing. No thrown weapon or missile weapon [should]{.highlight} ever be targeted at 
    your opponent's head or neck.

<details class="edit" open><summary></summary>
No thrown weapon or missile weapon ~~should~~ [may]{.new} ever be targeted at
your opponent's head or neck.
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

18. Arrows [should]{.highlight} be drawn with minimal pull necessary to score a successful
    hit. Bows should NEVER be used to parry an attack. As with thrown weapons,
	arrows should not be targeted at your opponent's head or neck.

<details class="edit" open><summary></summary>
Arrows ~~should~~ [must]{.new} be drawn with minimal pull necessary to score a
successful hit.
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

18. Arrows should be drawn with minimal pull necessary to score a successful
    hit. Bows [should]{.highlight} NEVER be used to parry an attack. As with thrown weapons,
	arrows should not be targeted at your opponent's head or neck.

<details class="edit" open><summary></summary>
Bows ~~should NEVER~~ [may not]{.new} be used to parry an attack.
</details>
:::::

::::: {.rule .block .part-2}
### 2.3 &sect; [The Rules We Fight By](./Omnibus.html#the-rules-we-fight-by) ###

18. Arrows should be drawn with minimal pull necessary to score a successful
    hit. Bows should NEVER be used to parry an attack. As with thrown weapons,
	arrows [should]{.highlight} not be targeted at your opponent's head or neck.

<details class="edit" open><summary></summary>
arrows ~~should~~ [must]{.new} not be targeted at your opponent's head or neck.
</details>
:::::