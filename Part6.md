::::: {.ideal .block .part-6}
### [6.1: Basic Magic Effects Everyone [Should]{.highlight} Know](./Omnibus.html#basic-magic-effects-everyone-should-know) ###

Even if you are not a spellcaster, and have no desire to become one, there is
still some basic information that you are responsible for understanding. In
nearly all cases, a spellcaster casting a spell on your PC [should]{.highlight} explain,
either through their Verbal Component or as an aside to you, how the spell
affects your PC. Even fighters [should]{.highlight} read the magic system and spell
descriptions, so they have a basic understanding of magic in the game and how it
might affect them. No spell effect may be ignored unless otherwise specified in
the spell system.
:::::

::::: {.ideal .block .part-6}
### 6.1 &sect; [Other Spells and Things You [Should]{.highlight} Know](./Omnibus.html#other-spells-and-things-you-should-know) ###
:::::

::::: {.suggestion .block .part-6}
### 6.2 &sect; [Learning a Spell](./Omnibus.html#learning-a-spell) ###

There are three ways to learn a spell. You [should]{.highlight} find a PC who knows the spell
and learn it from them. The PC who is teaching the spell must have the spell in
their spellbook, must be able to cast the spell at that event, and they must
sign the book with their character's name. If you cannot, or decide that you do
not want to learn it from a character who knows it, you can ask the EH to
provide you with a quest to learn a single spell. This quest will add a single
signature toward learning the spell. Additionally, you may teach yourself a
single spell if it is listed in your Spell Mastery section. Your PC does not
officially learn the spell until your teacher, teachers, or MM sign your
spellbook legibly. Your teacher or teachers are responsible for making sure you
understand all of the rules that go with the spell, and may refuse to sign if
you seem unable or unwilling to understand the rules. This is important, as the
teacher may be held liable for their student if they did not teach them the
spell properly.

<details class="edit" open><summary></summary>
You ~~should~~ [are encouraged to]{.new} find a PC who knows the spell and learn
it from them.
</details>
:::::

::::: {.rule .block .part-6}
### 6.5 &sect; [Enchanted Beings](./Omnibus.html#enchanted-beings) ###

All spellcasters, undead, and certain creatures are considered to be enchanted
beings. Normal fighters are only enchanted beings if under the effect of certain
spells, as per the [Undead Caveat](./Omnibus.html#undead). Enchanted beings are
affected by a certain number of spells, while non-enchanted beings are not.
These spells include [Circle of Protection](./Omnibus.html#circle-of-protection) and [Ward:
Enchanted Beings](./Omnibus.html#ward-enchanted-beings). By definition, any creature
[considered]{.highlight} undead is an enchanted being.

<details class="edit" open><summary></summary>
By definition, any ~~creature considered undead~~ [undead creature]{.new} is an
enchanted being.
</details>
:::::

::::: {.rule .block .part-6}
### 6.5 &sect; [Spell Sash](./Omnibus.html#spell-sash) ###

Some spells require a Spell Sash be worn to signify that a specific spell is
affecting the wearer. A Spell Sash may be constructed as a sash, a tabard, or a
belt favor. A Spell Sash must have the name of the spell clearly written on it.
Putting the Spell Sash on the target of the spell, or touching it if the target
is already wearing it, is considered to be an Active Component of the spell. A
Spell Sash is not stealable, and [should]{.highlight} stay on the target of the spell until
the spell is ended, at which time it is removed and returned to the 
spellcaster at the earliest convenient time. Either the spellcaster 
or the target may choose to end the spell by removing the Spell Sash, and it is 
also ended early if the Spell Sash is [disenchanted](./Omnibus.html#disenchant).
The Spell Sash must be worn in such a way that another player standing 5 feet in 
front of them can recognise it, and may not be covered unless the majority of their 
body is covered, such as by wearing a concealing cloak.

<details class="edit" open><summary></summary>
A Spell Sash is not stealable, and ~~should stay~~ [stays]{.new} on the target
of the spell until the spell is ended, at which time it is removed and returned
to the  spellcaster at the earliest convenient time.
</details>
:::::

::::: {.rule .block .part-6}
### 6.5 &sect; [Undead](./Omnibus.html#undead) ###

Some spells will make a recipient undead. When the spell is cast, the
spellcaster must explain to the target what it means to be undead. All undead
are affected by the spells: [Circle of Protection](./Omnibus.html#circle-of-protection),
[Ward: Enchanted Beings](./Omnibus.html#ward-enchanted-beings), and [Ward:
Undead](./Omnibus.html#ward-undead). A PC will remember what happened to them while they were
undead. While undead the PC [should]{.highlight} have the spellcaster's best intentions in
mind. A PC may be raised as undead if they are [diseased](./Omnibus.html#disease), but this
does not cure them of the disease.

<details class="edit" open><summary></summary>
While undead the PC ~~should have~~ [has]{.new} the spellcaster's best
intentions in mind.
</details>
:::::

::::: {.rule .block .part-6}
### 6.5 &sect; [Walking Dead](./Omnibus.html#walking-dead) ###

They [should]{.highlight} keep any items on them that they would retain if dragged, whether or
not the items are stealable. If anyone directly interferes with their movement
or attacks them, they fall to the ground and the effect ends. The Walking Dead
cannot take any actions other than walking. They may not attack, search other
bodies, cast spells, pick up objects, use magic items, drink potions, or perform
any other action aside from movement. The effect does not stop regeneration or
other methods of returning to life or becoming fully animated from occurring.
Becoming alive or fully animated as undead ends the Walking Dead effect. Corpses
under these effects are considered to be [undead](./Omnibus.html#undead).

<details class="edit" open><summary></summary>
They ~~should~~ keep any items on them that they would retain if dragged,
whether or not the items are stealable.
</details>
:::::

::::: {.rule .block .part-6}
### 6.5 &sect; [Wards](./Omnibus.html#wards) ###

All Ward spells are also [Chanting spells](./Omnibus.html#chanting).
Ward spells affect a specific type of creature listed in the name of the Ward.
When active, a Ward spell keeps the spellcaster from being attacked by creatures
affected by that spell. A Ward spell affects creatures in front of the
spellcaster, limited by a 180 degree hemisphere extending from shoulder to
shoulder and outwards from the chest. The Ward will not keep the targeted
creature(s) from walking around the spellcaster to attack others nearby. If the
Ward affects them, the targeted creature(s) must stay approximately five feet
away from the spellcaster, but need not retreat if the spellcaster advances upon
them. To cast a Ward, the spellcaster must hold their spell focus out toward the
targeted creatures while repeating the verbal. The verbal [should]{.highlight} make it clear
what creatures are affected by the Ward. For example, "Stay back undead. Stay
back undead. Stay back undead. Shoo," would be an appropriate verbal for the
[Ward: Undead](./Omnibus.html#ward-undead) spell. This spell will work for as long as the
spellcaster holds out the focus and keeps repeating the verbal. The people
playing the targeted creatures must be able to hear what the spellcaster is
saying, so it is up to the spellcaster to clearly and loudly chant their verbal.
While casting this spell, the spellcaster may not attack the targeted creatures.
A being that looks like it should be a targeted creature may not be (or may be
immune to the Ward). It is still the responsibility of the spellcaster to take
any and all weapon blows that hit them, even if the blows are from a creature
they believe should be affected by the Ward.

<details class="edit" open><summary></summary>
The verbal ~~should~~ [needs to]{.new} make it clear what creatures are affected
by the Ward.
</details>
:::::

::::: {.suggestion .block .part-6}
### 6.8 &sect; [Create Poison (4th Circle)](./Omnibus.html#create-poison) ###

The type of poison must be chosen when it is cast, and each individual effect
listed in the spellcaster's spellbook. The poison must be ingested and cannot
affect those who don't eat or drink it. This effect must be written legibly on a
scroll. This scroll is to be given to the victim by the spellcaster or MM
immediately after the MC has been consumed. Only one dose may be made per use,
and only the first person to ingest part of the MC is affected by a spell. The
spell is always rendered inert by [Immunity to Poison](./Omnibus.html#immunity-to-poison).
While the poison may have the intended effect to alter how someone may act, such
as a charm poison, it may in no way be used to compel any player to act in what
they consider an immoral or unethical fashion. A person who ingests a charm poison
always has the option of allowing their PC to die if they are OOC
uncomfortable with the situation. In any case, you [should]{.highlight} talk with the EH or MM
if the use of any poison bothers you.

<details class="edit" open><summary></summary>
In any case, you ~~should~~ [are encouraged to]{.new} talk with the EH or MM if
the use of any poison bothers you.
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Death Wish (4th Circle)](./Omnibus.html#death-wish) ###

This spell, when cast upon a dead body, implants a simple command into their
mind. The spellcaster must give the target player the scroll after completing
the verbal. The target [should]{.highlight} read the scroll and may refer to it at any time.
However, the scroll and the information it contains are OOC, and are not known
to the PC in any way. When the target character is alive, and hears the trigger
phrase, they must perform the command to the best of their ability. The spell
ends when the target successfully completes the command, or is slain trying. The
target may ignore any commands that violate the compulsions caveat.

<details class="edit" open><summary></summary>
The target ~~should~~ [must]{.new} read the scroll [at this time]{.new} and may
refer to it at any [later]{.new} time [as needed]{.new}.
</details>
:::::

::::: {.ideal .block .part-6}
### 6.8 &sect; [Enfeeble Being (3rd Circle)](./Omnibus.html#enfeeble-being) ###

This spell allows the spellcaster to remove the special powers and abilities
from a single NPC creature. To cast the spell, the spellcaster must get the
creature's attention and begin the verbal. Once the spell is completed, the
target may lose access to some or all of its abilities. This 
includes natural armor, spells, regeneration, etc. Because this is a relatively 
low-circle spell, it will probably have little or no effect on more 
powerful creatures, such as unique enemies or the proverbial "Big Bad Guy," but 
it might work on things like a troll, a lesser demon, or a goblin shaman. This 
spell will never work on PCs. A spellcaster [should]{.highlight} choose their targets wisely.
:::::

::::: {.ideal .block .part-6}
### 6.8 &sect; [Foretell (4th Circle)](./Omnibus.html#foretell) ###

Allows the spellcaster to make a prediction of an event to come, ie. "Sir Thomas
will slay a dragon with a silver sword." If the event foretold comes to pass the
EH/MM may grant a boon to the spellcaster or anyone involved in the prediction.
The nature and power of this boon is up to the EH/MM. The greater the foretold
event or more specific the prediction, the more powerful the resulting boon
[should]{.highlight} be.
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Group Healing (2nd Circle)](./Omnibus.html#group-healing) ###

To enchant the Group Healing circle, lay the rope(s) in a circle on the ground
with the ends touching. Then all the characters to be cast upon [should]{.highlight} be
gathered into the circle. The spellcaster(s) must then recite the VC, which
empowers the circle.

<details class="edit" open><summary></summary>
Then all the characters to be cast upon ~~should~~ [need to]{.new} be gathered
into the circle.
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Immunity to Poison (1st Circle)](./Omnibus.html#immunity-to-poison) ###

This spell makes the recipient immune to the next dose of poison that would have
otherwise affected their PC during the event. When damaged by the next poison
attack, whether ingested or delivered by a poisoned weapon, call "Immunity to
Poison!" Only one Immunity to Poison is used at a time. The recipient must take
any mundane damage from a poisoned weapon regardless of whether they are
protected from the actual poison. The recipient [should]{.highlight} be given the MC when the
spell is cast, and they should dispose of it when the immunity has been used.
More than one Immunity to Poison can be cast upon a recipient; the effect is
stackable. The MC of the spell is not stealable or transferable after it is
cast. This spell can also be cast as an antidote for any one poison that the
recipient has been subjected to, but in this case it will not provide any
further protection.

<details class="edit" open><summary></summary>
The recipient ~~should~~ [must]{.new} be given the MC when the spell is cast,
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Immunity to Poison (1st Circle)](./Omnibus.html#immunity-to-poison) ###

This spell makes the recipient immune to the next dose of poison that would have
otherwise affected their PC during the event. When damaged by the next poison
attack, whether ingested or delivered by a poisoned weapon, call "Immunity to
Poison!" Only one Immunity to Poison is used at a time. The recipient must take
any mundane damage from a poisoned weapon regardless of whether they are
protected from the actual poison. The recipient should be given the MC when the
spell is cast, and they [should]{.highlight} dispose of it when the immunity has been used.
More than one Immunity to Poison can be cast upon a recipient; the effect is
stackable. The MC of the spell is not stealable or transferable after it is
cast. This spell can also be cast as an antidote for any one poison that the
recipient has been subjected to, but in this case it will not provide any
further protection.

<details class="edit" open><summary></summary>
and they ~~should dispose~~ [are responsible for disposing]{.new} of it when the
immunity has been used.
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Intervention (6th Circle)](./Omnibus.html#intervention) ###

This spell allows the spellcaster to go to the EH and ask a boon from whatever
powers their magic. It [should]{.highlight} be cast in the presence of the EH or MM. It is to
be used to request favors such as: "Oh, please, great majestic god/Fire
Spirit/Navel Lint, grant me a quest to search for the lost soul of my overlord,
Sir Biff of Bonehead Ridge." This spell comes with no guarantee that the EH
won't simply listen to the request and say "No." This spell cannot create an
effect that will last beyond the end of the event, other than for healing
purposes. A spellcaster who uses drama and theatrics has a better chance of
success, and simple, small requests are also more likely to be granted. Any
requests that will unbalance the game will likely be either denied straight out,
or assigned an unsolvable quest.

<details class="edit" open><summary></summary>
It ~~should be~~ [only works if it is]{.new} cast in the presence of the EH or
MM.
</details>
:::::

::::: {.suggestion .block .part-6}
### 6.8 &sect; [Masterwork Hammer (6th Circle)](./Omnibus.html#masterwork-hammer) ###

This spell creates a Masterwork Hammer which the spellcaster may use
to repair non-armor, non-magic items (bows, weapons, shields) in 30 seconds. 
The spellcaster may also use the hammer to repair all armor on a target player
by using the hammer as the focus of the spell for 60 seconds. While using
the hammer to make any type of repair, the spellcaster cannot move their feet
and [should]{.highlight} actively use the hammer to simulate repairing the target. If the
hammer is broken or disenchanted, the spellcaster may repair it by holding the item in both
hands for 120 seconds.

<details class="edit" open><summary></summary>
the spellcaster cannot move their feet and ~~should~~ [is encouraged to]{.new}
actively use the hammer to simulate repairing the target.
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Reforge (5th Circle)](./Omnibus.html#reforge) ###

Silver Weapon
: This option will allow the spellcaster to create a permanent silver weapon.
  This option has an additional MC of the weapon's weight in silver coins (plain
  aluminum roofing tins, aluminum "Coin of the Realm," etc.). The spellcaster
  must write the words "Silver" and "Stealable" on the blade of the weapon. The
  spellcaster [should]{.highlight} also write "Silvered by" and the spellcaster's name on the
  blade. All of the silver that is collected for the casting of this spell must
  be handed over to the EH. All silver weapons are stealable, and the
  spellcaster must explain to anyone having a weapon made silver that it will be
  stealable, and will be considered property of the Realms, to be passed back
  and forth within the game as a searchable item, for as long as it is silver. A
  silver weapon will lose the quality of being silver if the writing on the
  blade fades to the point of no longer being readable or if the weapon is OOC
  or IC broken (such as by a boulder). Players may not protect the writing in
  any way and may not rewrite it. A silvered weapon broken IC (i.e., by any
  means other than the fading of the writing or the physical destruction of the
  prop) can be repaired by an expenditure of this spell, without having to
  provide the necessary silver. Repairing a weapon in this way does not allow
  you to remake the prop or rewrite the word "Silver" on the weapon. Silver
  Weapon will not allow you to silver an existing magic item.

<details class="edit" open><summary></summary>
The spellcaster ~~should~~ [must]{.new} also write "Silvered by" and the
spellcaster's name on the blade.
</details>
:::::

::::: {.suggestion .block .part-6}
### 6.8 &sect; [Repair Armor (1st Circle)](./Omnibus.html#repair-armor) ###

This spell will repair one hit location of armor. The AC [should]{.highlight} simulate
physically repairing the armor, such as tapping it with a focus, like a
boff-hammer.

<details class="edit" open><summary></summary>
~~The AC should~~ [You are encouraged to]{.new} simulate physically repairing
the armor,
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Ritual of Banishment (6th Circle)](./Omnibus.html#ritual-of-banishment) ###

Players who use drama and theatrics in their ritual are more likely of achieving
better results. This spell will only function upon NPCs. Once this spell is
attempted upon a creature, either in short or long formats, the spellcaster may
not cast this spell again for an hour. You are encouraged to use a small
timepiece to keep track of this time. Creatures that are shown to be too
powerful from the preparation ritual do not weaken the spellcaster. You may not
begin a preparation ritual on the same day as an uncompleted previous spell.
Upon completion of a more in-depth ritual, the spellcaster [should]{.highlight} ask the EH or
MM how long this spell is exhausted for, which may be the rest of the day. If
you know this spell, you must inform the EH or MM at check-in.

<details class="edit" open><summary></summary>
Upon completion of a more in-depth ritual, the spellcaster ~~should~~
[must]{.new} ask the EH or MM how long this spell is exhausted for,
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Seed of Life (6th Circle)](./Omnibus.html#seed-of-life) ###

Once the spell ends, the recipient [should]{.highlight} return the MC to the spellcaster as
soon as reasonably possible. Other than this, the MC is neither stealable nor
transferable in any way. If the recipient is rendered soulless, returned to
life, or raised as undead, the spell ends. If the recipient is diseased, this
spell will also cure them of their disease upon completion of the spell
(although the regeneration time will still be doubled from the effects of the
disease). This spell has no effect on the undead.

<details class="edit" open><summary></summary>
Once the spell ends, the recipient ~~should~~ [needs to]{.new} return the MC to
the spellcaster as soon as reasonably possible.
</details>
:::::

::::: {.suggestion .block .part-6}
### 6.8 &sect; [Shapeshifting (4th Circle)](./Omnibus.html#shapeshifting) ###

In addition, at the door of the event, the player is allowed to ask the EH or MM
to borrow an appropriate mask for the event in order to complete the illusion.
There is no guarantee that they will be able to provide the materials, so you
[should]{.highlight} bring your own.

<details class="edit" open><summary></summary>
There is no guarantee that they will be able to provide the materials, so you
~~should~~ [may wish to]{.new} bring your own.
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Transformation (6th Circle)](./Omnibus.html#transformation) ###

A complete description of the altered form must be in the spell description. The
spellcaster must alter their appearance when in the transformed state. They must
wear a different tabard, makeup, prosthetics, mask, or some other major
signifying indicator that they are "not quite right." Details [should]{.highlight} also be
listed in the spellcaster's spellbook.

<details class="edit" open><summary></summary>
Details ~~should~~ [must]{.new} also be listed in the spellcaster's spellbook.
</details>
:::::

::::: {.suggestion .block .part-6}
### 6.8 &sect; [Transformation (6th Circle)](./Omnibus.html#transformation) ###

* _Axe, Hammer, or Mace claws:_ 1 point. The spellcaster may call "Axe,"
  "Hammer," or "Mace" each time they swing their claws. The specific call must
  be chosen when the form is initially created. This option may only be taken
  once. The claws [should]{.highlight} reflect the nature of the call chosen.

<details class="edit" open><summary></summary>
~~The claws should~~ [The spellcaster is encouraged to make the claws]{.new}
reflect the nature of the call chosen.
</details>
:::::

::::: {.rule .block .part-6}
### 6.8 &sect; [Transmute Self (4th Circle)](./Omnibus.html#transmute-self) ###

This spell provides an immense amount of protection to the spellcaster, but also
requires an immense amount of concentration. This spell only takes effect once
the spellcaster has completed the VC once. While transmuted, the spellcaster is
completely immune to all forms of damage, magical or otherwise, regardless of
whether the material into which the spellcaster transmutes is vulnerable to any
form of damage. It does not make the spellcaster invisible or undetectable. The
spellcaster must choose what they are capable of attuning to when learning the
spell. Choices are: trees, stone, or earth. To transmute, the spellcaster must
embrace or lie down on the object they are capable of attuning to (so those who
can attune to trees hug a tree, to stone lie on or hug a rock, or to earth lie
on the ground). While transmuted, the spellcaster is "stuck" and cannot be
dragged. The object the spellcaster attunes with MUST be at least as massive as
the spellcaster. The spellcaster must keep their eyes closed and remain
perfectly still, and they must be constantly chanting their verbal while
transmuted. The spellcaster must chant loudly and clearly. If anything
interrupts the spellcaster's concentration, the spell is broken. OOC explanations 
(such as combat calls) do not interrupt this spell. For example, if a PC chanting 
a Transmute Self spell and is hit by a weapon, they may call "No effect" without 
interrupting the spell. As soon as the spellcaster moves, opens their eyes, 
or stops chanting, the spell ends. The spellcaster may not transmute for at least 
one slow 200 second count after regaining their proper form. The spellcaster [should]{.highlight}
use their common sense when deciding where to transmute. Pick a safe location, not 
the middle of a trail or a high combat area.

<details class="edit" open><summary></summary>
The spellcaster ~~should~~ [must]{.new} use their common sense when deciding
where to transmute.
</details>
:::::