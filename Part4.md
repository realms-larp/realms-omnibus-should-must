::::: {.suggestion .block .part-4}
### 4.1 &sect; [Being In-Character](./Omnibus.html#being-in-character) ###

Once an event has begun, you [are expected to]{.highlight} be in-character (IC) at all times.
This means that you are playing your PC the whole time you are at an event.
Staying IC can add greatly not only to your own event experience, but to those
who are playing the game around you. When a companion of your PC is "killed" it
adds to the tension and drama of the scene if they pretend they are dead, but it
breaks the mood completely if they are laughing and making comments.

<details class="edit" open><summary></summary>
Once an event has begun, you are ~~expected~~ [assumed]{.new} to be in-character
(IC) at all times.
</details>
:::::

:::: {.suggestion .block .part-4}
### 4.1 &sect; [Being In-Character](./Omnibus.html#being-in-character) ###

Your character may feel differently than you do about something, like slavery,
magic, politics, or religion. You may be a pacifist, while your character is a
bloodthirsty barbarian. If you can remain true to the character, despite your
differences, you can make a memorable story for yourself and those around you.
Sometimes staying IC is challenging, especially when you know something that
your character shouldn't logically know, but you [should]{.highlight} try to remain IC when
playing. Likewise, when the game is done, leave your character behind.

<details class="edit" open><summary></summary>
Sometimes staying IC is challenging, especially when you know something that
your character shouldn't logically know, but ~~you should~~ [it is worth it
to]{.new} try to remain IC when playing.
</details>
:::::

::::: {.suggestion .block .part-4}
### 4.1 &sect; [Breaking Character](./Omnibus.html#breaking-character) ###

Once an event has begun, breaking character [should]{.highlight} be done only when necessary.
If you must do so, what you say should be prefaced with "Out-of-character," as
in "Out-of-character, where is the tenting area?" That way, the person you are
addressing knows that it is a real-world concern, and should be dealt with
differently than a strictly IC concern.

<details class="edit" open><summary></summary>
Once an event has begun, breaking character ~~should be done only~~ [is
discouraged except]{.new} when necessary.
</details>
:::::

::::: {.suggestion .block .part-4}
### 4.1 &sect; [Breaking Character](./Omnibus.html#breaking-character) ###

Once an event has begun, breaking character should be done only when necessary.
If you must do so, what you say [should]{.highlight} be prefaced with "Out-of-character," as
in "Out-of-character, where is the tenting area?" That way, the person you are
addressing knows that it is a real-world concern, and should be dealt with
differently than a strictly IC concern.

<details class="edit" open><summary></summary>
If you must do so, [it helps if]{.new} what you say ~~should be~~ [is]{.new}
prefaced with "Out-of-character,"
</details>
:::::

::::: {.ideal .block .part-4}
### 4.1 &sect; [Breaking Character](./Omnibus.html#breaking-character) ###

Once an event has begun, breaking character should be done only when necessary.
If you must do so, what you say should be prefaced with "Out-of-character," as
in "Out-of-character, where is the tenting area?" That way, the person you are
addressing knows that it is a real-world concern, and [should]{.highlight} be dealt with
differently than a strictly IC concern.
:::::

::::: {.ideal .block .part-4}
### 4.1 &sect; [Out-of-Character-Only Terms](./Omnibus.html#out-of-character-only-terms) ###

There are a few terms that [should]{.highlight} only be used when speaking OOC: "Hold" and
"Medic." [Hold](./Omnibus.html#the-safety-rules) is only used in emergencies as it stops the game.
Medic is used when someone needs immediate real-world
medical attention for any reason: an allergic reaction to a bee-sting, a twisted
ankle, an asthma attack, etc. Do not call "Medic" for imaginary (IC) injuries.
If you need IC medical attention, call "Healer!"
:::::

::::: {.rule .block .part-4}
### 4.1 &sect; [Out-of-Play Areas and Time-Out](./Omnibus.html#out-of-play-areas-and-time-out) ###

If a fight breaks out in an area that is unsafe to fight in or that is
out-of-play, then the fight [should]{.highlight} be moved to a safe in-play area. If you are
in such an area, you may be asked to leave said area for combat. If you refuse
to leave the unsafe area, your PC is considered dead. After the fight, those
involved can move back to where the fight "really" took place, and continue on.

<details class="edit" open><summary></summary>
If a fight breaks out in an area that is unsafe to fight in or that is
out-of-play, then the fight ~~should~~ [needs to]{.new} be moved to a safe
in-play area.
</details>
:::::

::::: {.rule .block .part-4}
### [4.2: Dragging](./Omnibus.html#dragging) ###

To drag a dead, unconscious, or otherwise incapacitated body in our game, you
must place a hand on the shoulder, back, or arm of the body and say "Drag." The
player being dragged must then get up and walk with you, bringing what they are
carrying with them. Stealable items stay on a dragged corpse unless explicitly
searched off. A dragged body is considered one-handed and may not be used as a
weapon or a shield. At any time you may tell the dragged player "Drop," thereby
letting go of the player and dropping them on the ground. A body being dragged
can never be "thrown" or "tossed." If the person dragging the body lets go, then
the body [should]{.highlight} drop in place.

<details class="edit" open><summary></summary>
If the person dragging the body lets go, then the body ~~should drop~~
[drops]{.new} in place.
</details>
:::::

::::: {.rule .block .part-4}
### 4.3 &sect; [Searching](./Omnibus.html#searching) ###

Searching is a touchy subject. In the real world, if a bandit has just killed
someone they can just take everything they own. In the Realms, an object has to
be considered stealable to be taken from a person or location without the
owner's permission. The problem is that often these stealable items are not
easily recognizable, especially the smaller items. Also, while "secret pockets"
and such seem like a good idea at first, frisking a dead character could be
considered a form of harassment towards the player and [should]{.highlight} be avoided. To
handle this situation, the searching rule exists.

<details class="edit" open><summary></summary>
frisking a dead character could be considered a form of harassment towards the
player and ~~should~~ [is to]{.new} be avoided.
</details>
:::::

::::: {.rule .block .part-4}
### 4.3 &sect; [Full Search](./Omnibus.html#full-search) ###

The other way to search someone is to simulate taking your time to do it
thoroughly. That is, the character simulates stripping the body from head to
toe, ripping everything to shreds, garnering every last item you own, etc. In
order to do this the searcher simply says "Full search." Every stealable item
the victim has must be handed over to the searcher. The characters [should]{.highlight} take
120 seconds to do this. If so, then the items should be considered the IC
possessions of the searching character. Full searching will wake up an
unconscious character.

<details class="edit" open><summary></summary>
~~The characters should take~~ [This search takes]{.new} 120 seconds to ~~do
this~~ [perform]{.new}.
</details>
:::::

::::: {.rule .block .part-4}
### 4.3 &sect; [Full Search](./Omnibus.html#full-search) ###

The other way to search someone is to simulate taking your time to do it
thoroughly. That is, the character simulates stripping the body from head to
toe, ripping everything to shreds, garnering every last item you own, etc. In
order to do this the searcher simply says "Full search." Every stealable item
the victim has must be handed over to the searcher. The characters should take
120 seconds to do this. If so, then the items [should]{.highlight} be considered the IC
possessions of the searching character. Full searching will wake up an
unconscious character.

<details class="edit" open><summary></summary>
If so, then the items ~~should be considered~~ [are]{.new} the IC possessions of
the searching character.
</details>
:::::

::::: {.rule .block .part-4}
### 4.3 &sect; [Realms Thieves](./Omnibus.html#realms-thieves) ###

The only objects that are always in-play, are fair game for theft, and can be
stolen without consulting the bearer of the object, are those considered
stealable in-game (see [In-Game Items](./Omnibus.html#in-game-items), below). In
order to steal any other object, you must have the explicit permission of the
owner/bearer before making the theft. This means that to steal another PC's
jewelry (assuming that some of it is considered treasure), you must ask the
person who plays that PC. One way to do this is to kill or flat the character
and tell them that you are searching them. If they have anything that is
in-play, they must show it to you, for you to take or leave as you wish. You
[should]{.highlight} never pick up something off a table or from in front of someone's tent,
unless it is a magic or silver weapon, or Realms currency.

<details class="edit" open><summary></summary>
~~You should never~~ [Never]{.new} pick up something off a table or from in
front of someone's tent, unless it is a magic or silver weapon, or Realms
currency.
</details>
:::::

::::: {.suggestion .block .part-4}
### 4.4 &sect; [In-Game Items](./Omnibus.html#in-game-items) ###

The props for certain spells and items as designated by the EH are considered
"Event-Stealable," meaning that they are stealable treasure during an event, but
[should]{.highlight} be returned to their OOC owner when you leave an event. Before you leave
an event site, you must return (to the best of your ability) any items marked as
"Event-Stealable" to the EH or MM. Props for PC spells are returned by the EH or
MM as an OOC courtesy, and will be done so without revealing the identity of the
thief. If you have a spell that has an event-stealable prop as a component, you
may replace it without penalty at the next event if it is not returned to you
for whatever reason. You can never declare permanently stealable items as
event-stealable.

<details class="edit" open><summary></summary>
The props for certain spells and items as designated by the EH are considered
"Event-Stealable," meaning that they are stealable treasure during an event, but
should be returned to their OOC owner ~~when you leave~~ [at the end of]{.new}
an event.
</details>
:::::